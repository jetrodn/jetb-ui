<div id="top"></div>


[![MIT License][license-shield]][license-url]

# JETB UI

User interface for World Of Warcraft Shadowlands. Most of the imports are also available at [wago.io](https://wago.io/p/jetrodn)

![Screenshot #1](images/screenshot_1.jpg)

![Screenshot #2](images/screenshot_2.jpg)


## Addons list

* Advanced Interface Options
* Astral Keys
* BigWigs
* LittleWigs
* ChatBar Classic
* Combat Log Remover
* Details
* GTFO
* Leatrix Plus
* Lib: GroupInSpecT-1.1
* MinimapButtonBag
* Mythic Dungeon Tools
* MythicPlusTimer
* OmniCC
* OmniCD
* OPie
* Plater
* Raider IO
* SharedMedia (custom)
* WeakAuras
* WIM + Wim - ElvUI Skin
* ElvUI
* WA Soundpack by Causese


### ElvUI setting up 

The main profile you probably are looking for need is under `elvui/mage-profile.txt` file.

```
├── elvui
│   ├── mage-profile.txt main profile (main)
│   └── mage-private-settings.txt # private character settings (optional)
│   └── elvui-account-settings.txt # account settings (optional)
│   └── elvui-aura-filters.txt # aura filters (optional)
│   └── elvui-indicator-filter-style.txt # indicator filter styles (optional)
```

If you need help with importing ElvUI profiles please follow [Exporting/Importing ElvUI Profiles](https://www.tukui.org/forum/viewtopic.php?t=5)

[![Wago - ElvUI Profile][wago-shield]][wago-url]

### WeakAuras

All weakauras are under `wa` folder.

There are three group types of weakauras:

* general - just general weak auras for every class and role
* dungeons - dungeon weak auras, can be loaded only of certain class or group role
* classes - weak auras for different classes, will be loaded only for certain class

You can import whatever you actually need. File names should help you with understanding what a certain weak aura do.

For example: 

`0-explosive-tracker.txt` - track and count explosives killed by your team mates (dungeon affix)

`0-healer-wathcer.txt` - watch your healer mana percentage, soulstoned, outrange, dc status, etc.

Will not describe every weak aura, but if you have any questions about them DM / PM me.

### Plater

Includes only main profile import file under `plater/main-profile.txt`

If you need help with importing Plater profiles please follow [Plater FAQ](https://www.curseforge.com/wow/addons/plater-nameplates/pages/faq)


### Details

Include only main profile import file under `details/main-profile`


### Shared Media

It is partially custom addon that includes all fonts and custom textures that are used around all UI.

You have just to copy the folder `addons/SharedMedia` to your folder with World of Warcraft addons `%PATH_TO_GAME%/_retail_/Interface/AddOns`.


#### Links

* Wago: https://wago.io/p/jetrodn
* Twitch: https://twitch.tv/jetb
* Discord server: https://discord.com/invite/T4E8Cw4cT3

[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=flat-square
[license-url]: LICENCE.txt



[wago-shield]: https://media.wago.io/logo/57x57.png
[wago-url]: https://wago.io/uiVyOhlBg
